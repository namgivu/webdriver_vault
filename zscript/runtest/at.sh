#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

tc=${1:-"$AH/tests/webdriver_vault_2021/test_wd.py::Test::test_c03"}
#    1:- ie if $1 empty, use this

cd $AH
    PYTHONPATH=$AH  python3 -m pipenv run  \
        pytest $tc
