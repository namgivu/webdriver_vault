"""
ref. https://pypi.org/project/webdriver-manager/
"""

from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager


WD_IMPLICITLY_WAIT=6


def load_webdriver_f_autoinstall_firefox_wd():  # f aka from
    wd = webdriver.Firefox(executable_path=GeckoDriverManager().install())
    wd.implicitly_wait(WD_IMPLICITLY_WAIT)
    wd.maximize_window()
    return wd
