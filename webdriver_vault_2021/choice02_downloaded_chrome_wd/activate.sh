#!/usr/bin/env bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
    chrome_version=`google-chrome --version`

    chromedriver_version=`echo $chrome_version | cut -d' ' -f3 | cut -d'.' -f1`

    echo "Activating webdriver for $chrome_version..."
     chromedriver_f=`ls "$SH/vault" | grep $chromedriver_version`  # xx_f aka xx file
    chromedriver_fp="$SH/vault/$chromedriver_f"  # xx_fp aka xx full path
        if [ ! -f $chromedriver_fp ]; then echo "chromedriver not found for $chrome_version"; fi
        ln -sfn $chromedriver_fp "$SH/chromedriver"
            echo 'Your chromedriver is ready at'; ls -lahF "$SH/chromedriver" --color=always
