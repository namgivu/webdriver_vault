from webdriver_vault_2021.wd import load_webdriver, WEBDRIVER_CHOICE


class Test:

    def test_c01_chrome(self):
        wd = load_webdriver(choice=WEBDRIVER_CHOICE.c01__autoinstall_chrome_wd)  # wd aka webdriver
        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()

    def test_c01b_chrome(self):
        wd = load_webdriver(choice=WEBDRIVER_CHOICE.c01__autoinstall_chrome_wd__headless)  # wd aka webdriver
        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()

    def test_c01_firefox(self):
        wd = load_webdriver(choice=WEBDRIVER_CHOICE.c01b__autoinstall_firefox_wd)  # wd aka webdriver
        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()

    def test_c02(self):
        wd = load_webdriver(choice=WEBDRIVER_CHOICE.c02__downloaded_chrome_wd)  # wd aka webdriver
        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()

    def test_c03(self):
        wd = load_webdriver(choice=WEBDRIVER_CHOICE.c03__headless_standalone_chromewd_3_14)  # wd aka webdriver
        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()
