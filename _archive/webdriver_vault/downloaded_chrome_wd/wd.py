import os
from selenium import webdriver


WD_IMPLICITLY_WAIT=6


def load_webdriver_f_downloaded_chromewd():
    # load CHROMEDRIVER_BINARY
    THIS_FOLDER = os.path.abspath(__file__ + '/..')
    CHROMEDRIVER_BINARY = f'{THIS_FOLDER}/chromedriver'
    if not os.path.isfile(CHROMEDRIVER_BINARY): raise Exception(f'Not found chromedriver binary at {CHROMEDRIVER_BINARY} - you may want to run activate.sh')

    # create wd
    wd = webdriver.Chrome(executable_path=CHROMEDRIVER_BINARY)  # ref. https://stackoverflow.com/a/42478941/248616
    wd.implicitly_wait(WD_IMPLICITLY_WAIT)
    wd.maximize_window()
    return wd
