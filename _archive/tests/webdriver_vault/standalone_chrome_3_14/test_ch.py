from webdriver_vault.headless_standalone_chromewd_3_14.wd import load_webdriver_f_headless_standalone_chromewd

def test():
    wd = load_webdriver_f_headless_standalone_chromewd()
    wd.get('http://www.google.com')
    assert wd.title == 'Google'
    wd.quit()
