from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class Test:
    """
    required 4444 wd - available at ./webdriver_vault/standalone-chrome-3.141/up.sh
    """


    def test_chrome(self):
        SELENIUM_HUB = 'http://localhost:4444/wd/hub'; wd = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.CHROME,
        )

        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()


    def test_firefox(self):
        SELENIUM_HUB = 'http://localhost:4444/wd/hub'; wd = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX,
        )

        wd.get('http://www.google.com')
        assert wd.title == 'Google'
        wd.quit()
